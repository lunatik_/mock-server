package mock.server.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/")
public class IndexController {

    @Value("${my.test.parameter}")
    private String MY_PARAMETER_VALUE;

    @PostMapping
    public ResponseEntity<String> index(@RequestBody String request, @RequestHeader Map<String, String> headers) {
        System.out.println("Request body:\n" + request);
        return new ResponseEntity<>("hello from container!</br>my.test.parameter = " + MY_PARAMETER_VALUE, HttpStatus.OK);
    }

    @GetMapping
    public @ResponseBody ResponseEntity<String> index() {
        return new ResponseEntity<>("hello from container!</br>my.test.parameter = " + MY_PARAMETER_VALUE, HttpStatus.OK);
    }

}
