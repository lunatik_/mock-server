### 1) Создать имеджстрим
```shell
oc create -f 1_imageStream.yml
```

### 2) Создать buildConfig
```shell
oc create -f 2_buildConfig_buildImage.yml
```
Сборка запустилась сразу, т.к есть триггер ImageChange, посмотреть ее можно так: 
```shell
oc get builds
oc delete buildconfig spring-builder
```

### 3) Создать deploymentConfig
```shell
oc create -f 3_deploymentConfig.yml
```
Расшарить
```shell
oc expose dc/spring-boot
oc expose svc/spring-boot
```