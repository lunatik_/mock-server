FROM openjdk:8-jdk-alpine as build
RUN adduser -D -s /bin/sh myuser
WORKDIR /home/myuser

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src

RUN chmod 755 mvnw && chown myuser:myuser mvnw
RUN chmod -R 755 .mvn && chown -R myuser:myuser .mvn
RUN chmod 755 pom.xml && chown myuser:myuser pom.xml
RUN chmod -R 755 src && chown -R myuser:myuser src
USER myuser

RUN ./mvnw package -DskipTests
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG DEPENDENCY=/home/myuser/target/dependency
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app
ENTRYPOINT ["java","-cp","app:app/lib/*","mock.server.Application"]